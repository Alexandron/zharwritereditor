package com.alexzharikov34.zharwritereditor;

import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;


/**
 * Created by Саша on 30.06.2018.
 */

public class SaveFileChooser extends FileChooser {



    public interface FileSavedListener {
        void fileSaved(File file);
    }

    private FileSavedListener saveListener;

    public SaveFileChooser setSaveListener(FileSavedListener saveListener) {
        this.saveListener = saveListener;
        return this;
    }


    @Override
    public FileChooser setFileListener(FileChooser.FileSelectedListener fileListener) {
        throw new UnsupportedOperationException("Операция выбора файла не поддерживается в данном классе");
    }

    private String text;

    public SaveFileChooser setText(String text) {
        this.text = text;
        return this;
    }


    public SaveFileChooser(final Activity activity, final String pattern) {
        super(activity);
        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_button_layout, null);
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.button_layout);
        final Button saveFile = (Button) view.findViewById(R.id.saveFile);
        final EditText saveFileName = (EditText) view.findViewById(R.id.saveFileName);
        saveFileName.setText(String.valueOf(saveFileName.getText()) + pattern);
        saveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File newFile = new File(currentPath + "/" + saveFileName.getText());
                try {
                    newFile.createNewFile();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile.getAbsolutePath()), "windows-1251"));
                    writer.write(text);
                    writer.close();
                    dialog.dismiss();
                    if (saveListener != null) {
                        saveListener.fileSaved(newFile);
                    }
                    Snackbar.make((ConstraintLayout) activity.findViewById(R.id.main_layout), "Новый файл " + saveFileName.getText() + " успешно сохранён", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Snackbar.make((ConstraintLayout) activity.findViewById(R.id.main_layout), "Ошибка " + e.getMessage(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        });
        dialog.addContentView(relativeLayout,lp);
        LinearLayout button_linear_layout = (LinearLayout) view.findViewById(R.id.button_linear_layout);
        View fakeView = inflater.inflate(R.layout.fake_layout,null);
        LinearLayout fakeLinearLayout = (LinearLayout) fakeView.findViewById(R.id.layout_fake);
        fakeLinearLayout.setMinimumHeight(button_linear_layout.getHeight());
        list.addFooterView(fakeLinearLayout);
    }



    @Override
    protected void refresh(final File path) {
        this.currentPath = path;
        if (path.exists()) {
            File[] dirs = path.listFiles(new FileFilter() {
                @Override public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = path.listFiles(new FileFilter() {
                @Override public boolean accept(File file) {
                    if (!file.isDirectory()) {
                        if (!file.canRead()) {
                            return false;
                        } else if (extension == null) {
                            return true;
                        } else {
                            return file.getName().toLowerCase().endsWith(extension);
                        }
                    } else {
                        return false;
                    }
                }
            });

            // convert to an array
            int i = 0;
            final String[] fileList;
            if (path.getParentFile() == null) {
                fileList = new String[dirs.length];
            } else {
                fileList = new String[dirs.length + 1];
                fileList[i++] = PARENT_DIR;
            }
            Arrays.sort(dirs);
            for (File dir : dirs) fileList[i++] = dir.getName();

            // refresh the user interface
            dialog.setTitle(currentPath.getPath());
            list.setAdapter(new ArrayAdapter(activity,
                    android.R.layout.simple_list_item_1, fileList) {
                @Override public View getView(int pos, View view, ViewGroup parent) {
                    view = super.getView(pos, view, parent);
                    ((TextView) view).setSingleLine(true);
                    File file = new File(path.getPath() + "/" + fileList[pos]);
                        setDrawable(folderIcon, (TextView) view);

                    return view;
                }
            });
        }
    }

    @Override
    protected File getChosenFile(String fileChosen) throws NullPointerException {
        if (fileChosen != null) {
            if (fileChosen.equals(PARENT_DIR)) {
                return currentPath.getParentFile();
            } else {
                return new File(currentPath, fileChosen);
            }
        }
        return null;
    }

}
