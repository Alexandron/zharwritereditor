package com.alexzharikov34.zharwritereditor;

/**
 * Created by Саша on 30.06.2018.
 */

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;

public class FileChooser {
    protected static final String PARENT_DIR = "..";

    protected final Activity activity;
    protected ListView list;

    protected File currentPath;

    // filter on file extension
    protected String extension = null;
    protected Drawable folderIcon;
    protected Drawable fileIcon;



    public FileChooser setIconFolder(Drawable drawable) {
        this.folderIcon = drawable;
        return this;
    }

    public FileChooser setIconFile(Drawable drawable) {
        this.fileIcon = drawable;
        return this;
    }


    public void setExtension(String extension) {
        this.extension = (extension == null) ? null :
                extension.toLowerCase();
    }

    // file selection event handling
    public interface FileSelectedListener {
        void fileSelected(File file);
    }
    public FileChooser setFileListener(FileSelectedListener fileListener) {
        this.fileListener = fileListener;
        return this;
    }
    private FileSelectedListener fileListener;

    protected Dialog dialog;

    public FileChooser(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);
        list = new ListView(activity);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int which, long id) {
                String fileChosen = (String) list.getItemAtPosition(which);
                File chosenFile = getChosenFile(fileChosen);
                if (chosenFile != null) {
                    if (chosenFile.isDirectory()) {
                        refresh(chosenFile);
                    } else {
                        if (fileListener != null) {
                            fileListener.fileSelected(chosenFile);
                        }
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.setContentView(list);
        dialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        refresh(Environment.getExternalStorageDirectory());
    }

    public void showDialog() {
        dialog.show();
    }


    /**
     * Sort, filter and display the files for the given path.
     */
    protected void refresh(final File path) {
        this.currentPath = path;
        if (path.exists()) {
            File[] dirs = path.listFiles(new FileFilter() {
                @Override public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = path.listFiles(new FileFilter() {
                @Override public boolean accept(File file) {
                    if (!file.isDirectory()) {
                        if (!file.canRead()) {
                            return false;
                        } else if (extension == null) {
                            return true;
                        } else {
                            return file.getName().toLowerCase().endsWith(extension);
                        }
                    } else {
                        return false;
                    }
                }
            });

            // convert to an array
            int i = 0;
            final String[] fileList;
            if (path.getParentFile() == null) {
                fileList = new String[dirs.length + files.length];
            } else {
                fileList = new String[dirs.length + files.length + 1];
                fileList[i++] = PARENT_DIR;
            }
            Arrays.sort(dirs);
            Arrays.sort(files);
            for (File dir : dirs) fileList[i++] = dir.getName();
            for (File file : files ) fileList[i++] = file.getName();

            // refresh the user interface
            dialog.setTitle(currentPath.getPath());
            list.setAdapter(new ArrayAdapter(activity,
                    android.R.layout.simple_list_item_1, fileList) {
                @Override public View getView(int pos, View view, ViewGroup parent) {
                    view = super.getView(pos, view, parent);
                    ((TextView) view).setSingleLine(true);
                    File file = new File(path.getPath() + "/" + fileList[pos]);
                    if (file.isDirectory()) {
                        setDrawable(folderIcon, (TextView) view);
                    } else {
                        setDrawable(fileIcon, (TextView) view);
                    }

                    return view;
                }
            });
        }
    }

    protected void setDrawable(Drawable drawable, TextView view) {
        if (view != null) {
            if (drawable != null) {
                drawable.setBounds(0, 0, 60, 60);
                view.setCompoundDrawables(drawable, null, null, null);
            } else view.setCompoundDrawables(null, null, null, null);
        }
    }


    /**
     * Convert a relative filename into an actual File object.
     */
    protected File getChosenFile(String fileChosen) {
        if (fileChosen.equals(PARENT_DIR)) {
            return currentPath.getParentFile();
        } else {
            return new File(currentPath, fileChosen);
        }
    }
}
