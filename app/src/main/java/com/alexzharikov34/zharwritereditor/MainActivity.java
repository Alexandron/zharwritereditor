package com.alexzharikov34.zharwritereditor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skydoves.colorpickerpreference.ColorEnvelope;
import com.skydoves.colorpickerpreference.ColorListener;
import com.skydoves.colorpickerpreference.ColorPickerDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static String activeFile;
    public static boolean TEXT_STYLE_CHANGED_INTO_SPANNABLE = false;
    public static int OPEN_ACTION = 0;
    public TextView wordsCounter;
    public EditText Input;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        wordsCounter = (TextView) findViewById(R.id.wordsCounter);
        Input = (EditText) findViewById(R.id.Input);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countWords(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
        }
    }

    private void countWords(String s) {
        String str = s.trim();
        if (str.length() == 0){
            wordsCounter.setText("Слов: 0");
        } else {
            int spaceCount = 0;
            for (char c: str.toCharArray()) {
                if (c == ' ') {
                    spaceCount++;
                }
            }
            wordsCounter.setText(String.format("Cлов: %s", new Object[]{String.valueOf(spaceCount+1)}));
        }
    }

    private boolean checkStyled(String path) {
        Boolean result = false;
        String extension;
        int i = path.lastIndexOf(".");
        if (i > 0) {
            extension = path.substring(i+1);
            Log.d("EXT",extension);
            if (Objects.equals(extension, "html")) {
                try {
                    FileReader fr = new FileReader(new File(path));
                    BufferedReader br = new BufferedReader(fr);
                    if (Objects.equals(br.readLine(), "<!--zwe-->")) {
                        result = true;
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }  else {
                Log.d("Equals","NoE");
            }

        }
        return result;

    }

    public void open() {
        if ((Input.getText().toString().trim().length() != 0) & (OPEN_ACTION == 0)) {
            OPEN_ACTION = 1;
            createFile().show();
        } else if ((Input.getText().toString().trim().length() == 0) | (OPEN_ACTION == 2)) {
            new FileChooser(MainActivity.this).setFileListener(new FileChooser.FileSelectedListener() {
                @Override public void fileSelected(final File file) {
                    try {
                        activeFile = file.getAbsolutePath();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"windows-1251"));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line + "\n");
                        }
                        TEXT_STYLE_CHANGED_INTO_SPANNABLE = false;
                        if (!checkStyled(activeFile)) {
                            Input.setText(stringBuilder.toString());
                        } else {
                            Log.d("Spannable", "Yes");
                            SpannableStringBuilder ssb = new SpannableStringBuilder(Html.fromHtml(stringBuilder.toString()));
                            Input.setText(new SpannableString(ssb), EditText.BufferType.SPANNABLE);
                            TEXT_STYLE_CHANGED_INTO_SPANNABLE = true;
                        }
                        bufferedReader.close();
                        countWords(stringBuilder.toString());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }}).setIconFolder(MainActivity.this.getDrawable(R.drawable.open_folder))
                    .setIconFile(MainActivity.this.getDrawable(R.drawable.file_icon))
                    .showDialog();
            OPEN_ACTION = 0;
        }
    }

    public void savePlainText(String text) {
        if (activeFile == null) {
            saveNew(text,".txt");
        } else {
            try {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(activeFile), "windows-1251"));
                writer.write(text);
                writer.close();
                Snackbar.make((ConstraintLayout) findViewById(R.id.main_layout), "Сохранено", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        if (TEXT_STYLE_CHANGED_INTO_SPANNABLE) saveSpannable((Spannable) Input.getText()); else {
            savePlainText(String.valueOf(Input.getText()));
        }
    }

    private void saveSpannable(Spannable text) {
         String saveString = Html.toHtml(new SpannedString(text)).toString();
         saveString = "<!--zwe-->\n" + saveString;
         savePlainText(saveString);
    }

    public void saveNew(String text, String saveType) {
        new SaveFileChooser(MainActivity.this, saveType)
        .setText(text).setSaveListener(new SaveFileChooser.FileSavedListener() {
            @Override
            public void fileSaved(File file) {
                activeFile = file.getAbsolutePath();
            }
        }).setIconFolder(MainActivity.this.getDrawable(R.drawable.open_folder))
                .setIconFile(MainActivity.this.getDrawable(R.drawable.file_icon))
                .showDialog();
    }

    public android.app.AlertDialog askClear() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Вы уверены?");
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Input.setText(null);
            }
        }).setNegativeButton("ОТМЕНА", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        return builder.create();
    }

    public AlertDialog createFile() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Сохранить набранный текст?");
        builder.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                save();
                Input.setText(null);
                activeFile = null;
                TEXT_STYLE_CHANGED_INTO_SPANNABLE = false;
                if (OPEN_ACTION == 1) {
                    OPEN_ACTION = 2;
                    open();
                }
            }
        }).setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Input.setText(null);
                activeFile = null;
                TEXT_STYLE_CHANGED_INTO_SPANNABLE = false;
                if (OPEN_ACTION == 1) {
                    OPEN_ACTION = 2;
                    open();
                }
            }
        }).setNeutralButton("ОТМЕНА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (OPEN_ACTION == 1) {
                    OPEN_ACTION = 0;
                }
            }
        });
        return builder.create();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.Save) {
            EditText Input = (EditText) findViewById(R.id.Input);
           save();
        } else if (id == R.id.Open) {
            open();

        } else if (id == R.id.NewFile) createFile().show();



        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_last) {
            // Handle the camera action
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_prj_create) {

        } else if (id == R.id.nav_prj_final) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_rate) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void ClearOnClick(View view) {
        askClear().show();
    }

    private AlertDialog textStyleSwitchDialogSaveOld() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Сохранить написанный текст?");
        builder.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                savePlainText(String.valueOf(Input.getText()));
                styleSwitchFinal();
            }
        }).setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                styleSwitchFinal();
            }
        });
        return builder.create();
    }

    private void styleSwitchFinal() {
        SpannableStringBuilder ssb = new SpannableStringBuilder(String.valueOf(Input.getText()));
        Spannable spannable = new SpannableString(ssb);
        Input.setText(spannable, TextView.BufferType.SPANNABLE);
        TEXT_STYLE_CHANGED_INTO_SPANNABLE = true;
        String saveString = Html.toHtml(new SpannedString(spannable)).toString();
        saveString = "<!--zwe-->\n" + saveString;
        saveNew(saveString, ".html");
    }

    private AlertDialog textStyleSwitchDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Вы находитесь в режиме простого текста.\nДанные изменения потребуют\nпересохранения в формате html.\nПодтвердить?");
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (Input.getText() != null) textStyleSwitchDialogSaveOld().show();
                else styleSwitchFinal();
            }
        }).setNegativeButton("ОТМЕНА", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        return builder.create();
    }

    private AlertDialog setColor(final Spannable spannable, final int s, final int e) {
        ColorPickerDialog.Builder builder = new ColorPickerDialog.Builder(this,AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setTitle("Выберите цвет: ");
        builder.setPreferenceName("MyColorPickerDialog");
        builder.setPositiveButton("ОК",new ColorListener() {
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope) {
                int[] result = colorEnvelope.getColorRGB();
                ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(result[0],result[1],result[2]));
                spannable.setSpan(fcs,s,e,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                Input.setText(null);
                Input.append(spannable);
                Input.setSelection(s);
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    public void spannableTextChanging(View view) {
        if (TEXT_STYLE_CHANGED_INTO_SPANNABLE) {
            final EditText editText = Input;
            final int startSelection = editText.getSelectionStart();
            int endSelection = editText.getSelectionEnd();
            SpannableStringBuilder ssb = new SpannableStringBuilder(editText.getText());
            Spannable sb = new SpannableString(ssb);
            Editable edBefore = (Editable) editText.getText().subSequence(0,startSelection);
            String stThis = String.valueOf(editText.getText()).substring(startSelection,endSelection);
            Editable edAfter = (Editable) editText.getText().subSequence(endSelection,sb.length()-1);

            if ((startSelection >= 0) && (endSelection >= 0)) {
                switch (view.getTag().toString()) {
                    case "B":
                        sb.setSpan(new StyleSpan(Typeface.BOLD), startSelection, endSelection, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        break;
                    case "I":
                        sb.setSpan(new StyleSpan(Typeface.ITALIC), startSelection, endSelection, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        break;
                    case "U":
                        sb.setSpan(new UnderlineSpan(), startSelection, endSelection, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        break;
                    case "S":
                        sb.setSpan(new StrikethroughSpan(), startSelection, endSelection, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        break;
                    case "N":
                        sb = new SpannableString(TextUtils.concat(edBefore,stThis,edAfter));
                        break;
                    case "C":
                        setColor(sb,startSelection,endSelection).show();
                        break;
                }
                if (view.getTag().toString() != "C") {
                    editText.setText(null);
                    editText.append(sb);
                    editText.setSelection(startSelection);
                }
            }
        } else {
            textStyleSwitchDialog().show();
        }
    }
}

